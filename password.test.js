const { checkLength, checkAlphabet, checkSymbol, checkPassword, checkDigit } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })

  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })

  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })

  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })

  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has digits 0 in password', () => {
    expect(checkDigit('0')).toBe(true)
  })

  test('should has digits 9 in password', () => {
    expect(checkDigit('9')).toBe(true)
  })
})

describe('Test Symbol', () => {
  test('should has symbol ! in password', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })

  test('should has symbol ) in password', () => {
    expect(checkSymbol('11)11')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should password Ww@1 to be false', () => {
    expect(checkPassword('Ww@1')).toBe(false)
  })
})
