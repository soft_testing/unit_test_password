const checkLength = function (password) {
  return password.length >= 8 && password.length <= 25
}

const checkAlphabet = function (password) {
  const alphabet = 'abcdefghijklmnopqrstuvwxyz'
  for (const ch of password) {
    if (alphabet.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkDigit = function (password) {
  const digits = '0123456789'
  for (const ch of digits) {
    if (digits.includes(ch.toLowerCase())) return true
  }
  return false
}
const checkSymbol = function (password) {
  const symbols = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of symbols) {
    if (symbols.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkPassword = function (password) {
  return checkLength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)
}
module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
